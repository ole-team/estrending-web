<?php
/**
 * Database Class
 *
 * @package jnews-pay-writer
 * @author Jegtheme
 * @since 10.0.0
 */

namespace JNews\AMP\Ads;

use JNews\AMP\Helper;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Database
 * phpcs:disable WordPress.DB.PreparedSQL,Squiz.Commenting.FunctionComment.MissingParamComment
 */
class JNews_AMP_Ads {

	/**
	 * Class instance
	 *
	 * @var Ads
	 */
	private static $instance;

	/**
	 * Return class instance
	 *
	 * @return Ads
	 */
	public static function instance() {
		if ( null === static::$instance ) {
			static::$instance = new static();
		}

		return static::$instance;
	}

	/**
	 * @var array
	 */
	public $amp_ads = array();

	/**
	 * Class constructor
	 */
	private function __construct() {
		$this->setup_init();
		$this->setup_hook();
	}

	protected function setup_init() {
		$locations = array( 'above_header', 'above_article', 'below_article', 'above_content', 'inline_content', 'below_content' );

		foreach ( $locations as $location ) {
			$enable_ad = Helper::jnews_get_option( 'amp_ads_' . $location . '_enable', false );

			if ( $enable_ad ) {
				$this->amp_ads[ $location ] = true;
			}
		}
	}

	/**
	 * Setup hook
	 */
	protected function setup_hook() {
		add_action( 'jnews_amp_before_header', array( $this, 'above_header_ads' ) );
		add_action( 'jnews_amp_before_header', array( $this, 'google_auto_ads' ) );
		add_action( 'jnews_amp_before_article', array( $this, 'above_article_ads' ) );
		add_action( 'jnews_amp_after_article', array( $this, 'below_article_ads' ) );
		add_action( 'jnews_amp_before_content', array( $this, 'above_content_ads' ) );
		add_action( 'jnews_amp_after_content', array( $this, 'below_content_ads' ) );
	}


	public function above_header_ads() {
		$location = 'above_header';

		if ( array_key_exists( $location, $this->amp_ads ) ) {
			$html = "<div class=\"amp_ad_wrapper jnews_amp_{$location}_ads\">" . $this->render_ads( $location ) . '</div>';

			echo jnews_sanitize_by_pass( $html );
		}
	}

	/**
	 * Google Auto Ads
	 */
	public function google_auto_ads() {
		if ( Helper::jnews_get_option( 'amp_ads_google_auto_enable', false ) ) {
			$publisher_id = Helper::jnews_get_option( 'amp_ads_google_auto_publisher', false );

			if ( $publisher_id ) {
				$html = "<amp-auto-ads type=\"adsense\" data-ad-client=\"{$publisher_id}\"></amp-auto-ads>";
				echo jnews_sanitize_by_pass( $html );
			}
		}
	}

	/**
	 * Above article ads
	 */
	public function above_article_ads() {
		$location = 'above_article';

		if ( array_key_exists( $location, $this->amp_ads ) ) {
			$html = "<div class=\"amp_ad_wrapper jnews_amp_{$location}_ads\">" . $this->render_ads( $location ) . '</div>';

			echo jnews_sanitize_by_pass( $html );
		}
	}

	/**
	 * Below article ads
	 */
	public function below_article_ads() {
		$location = 'below_article';

		if ( array_key_exists( $location, $this->amp_ads ) ) {
			$html = "<div class=\"amp_ad_wrapper jnews_amp_{$location}_ads\">" . $this->render_ads( $location ) . '</div>';

			echo jnews_sanitize_by_pass( $html );
		}
	}

	/**
	 * Above content ads
	 */
	public function above_content_ads() {
		$location = 'above_content';

		if ( array_key_exists( $location, $this->amp_ads ) ) {
			$html = "<div class=\"amp_ad_wrapper jnews_amp_{$location}_ads\">" . $this->render_ads( $location ) . '</div>';

			echo jnews_sanitize_by_pass( $html );
		}
	}

	/**
	 * Below content ads
	 */
	public function below_content_ads() {
		$location = 'below_content';

		if ( array_key_exists( $location, $this->amp_ads ) ) {
			$html = "<div class=\"amp_ad_wrapper jnews_amp_{$location}_ads\">" . $this->render_ads( $location ) . '</div>';

			echo jnews_sanitize_by_pass( $html );
		}
	}

	/**
	 * Inline content ads
	 */
	public function inline_content_ads( $content ) {
		if ( function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() ) {
			$location = 'inline_content';

			if ( array_key_exists( $location, $this->amp_ads ) ) {
				$tag     = new \JNews\ContentTag( $content );
				$pnumber = $tag->total( 'p' );

				$adsposition = Helper::jnews_get_option( 'amp_ads_' . $location . '_paragraph', 3 );
				$adsrandom   = Helper::jnews_get_option( 'amp_ads_' . $location . '_paragraph_random', false );

				if ( $adsrandom ) {
					if ( is_array( $pnumber ) ) {
						$maxparagraph = count( $pnumber ) - 2;
						$adsposition  = rand( $adsposition, $maxparagraph );
					}
				}

				$html    = "<div class=\"amp_ad_wrapper jnews_amp_{$location}_ads\">" . $this->render_ads( $location ) . '</div>';
				$content = $this->prefix_insert_after_paragraph( $html, $adsposition, $tag );
			}
		}

		return $content;
	}

	/**
	 * Render ads
	 *
	 * @param  string $location
	 *
	 * @return string
	 */
	protected function render_ads( $location ) {
		$ads_html = '';

		if ( Helper::jnews_get_option( 'amp_ads_' . $location . '_type', 'googleads' ) == 'googleads' ) {
			$publisherid = Helper::jnews_get_option( 'amp_ads_' . $location . '_google_publisher', '' );
			$slotid      = Helper::jnews_get_option( 'amp_ads_' . $location . '_google_id', '' );

			$publisherid = str_replace( ' ', '', $publisherid );
			$slotid      = str_replace( ' ', '', $slotid );

			if ( ! empty( $publisherid ) && ! empty( $slotid ) ) {
				$ad_size = Helper::jnews_get_option( 'amp_ads_' . $location . '_size', 'auto' );

				if ( $ad_size !== 'auto' ) {
					$ad_size = explode( 'x', $ad_size );
				} else {
					$ad_size = array( '320', '50' );
				}

				$publisherid = str_replace( 'ca-', '', $publisherid );

				$gdpr_consent = Helper::jnews_get_option( 'amp_gdpr_enable', false ) ? 'data-block-on-consent="_till_accepted"' : '';

				$ads_html .=
					"<amp-ad
                        {$gdpr_consent}
                        type=\"adsense\"
                        width={$ad_size[0]} 
                        height={$ad_size[1]}
                        data-ad-client=\"ca-{$publisherid}\"
                        data-ad-slot=\"{$slotid}\">
                    </amp-ad>";
			}
		} else {
			$ads_html = Helper::jnews_get_option( 'amp_ads_' . $location . '_custom', '' );
		}

		return $ads_html;
	}

}

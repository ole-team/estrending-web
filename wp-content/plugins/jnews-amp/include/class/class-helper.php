<?php
/**
 * @author Jegtheme
 */

namespace JNews\AMP;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Helper
 */
class Helper {

	public static function jnews_get_option( $setting, $default = null ) {
		$options = get_option( 'jnews_option', array() );
		$value   = $default;

		if ( isset( $options[ $setting ] ) ) {
			$value = $options[ $setting ];
		}

		return $value;
	}

	public static function jnews_print_translation( $string, $domain, $name ) {
		do_action( 'jnews_print_translation', $string, $domain, $name );
	}

	public static function jnews_return_translation( $string, $domain, $name, $escape = true ) {
		return apply_filters( 'jnews_return_translation', $string, $domain, $name, $escape );
	}

}

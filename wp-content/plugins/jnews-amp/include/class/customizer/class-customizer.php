<?php
/**
 * JNews  Class
 *
 * @author Jegtheme
 * @since 1.0.0
 * @package jnews-pay-writer
 */

namespace JNews\AMP\Customizer;

/**
 * Class Customizer
 *
 * @package JNews\Paywall\Customizer
 */
class Customizer {
	/**
	 * @var Customizer
	 */
	private static $instance;

	/**
	 * @var
	 */
	private $customizer;

	/**
	 * Customizer constructor.
	 */
	private function __construct() {

		// actions.
		add_action( 'jeg_register_customizer_option', array( $this, 'customizer_option' ) );

		// filters.
		add_filter( 'jeg_register_lazy_section', array( $this, 'autoload_section' ) );

		add_filter( 'jnews_global_gdpr_option', array( $this, 'amp_gdpr_options' ) );
	}

	/**
	 * @return Customizer
	 */
	public static function instance() {
		if ( null === static::$instance ) {
			static::$instance = new static();
		}

		return static::$instance;
	}

	/**
	 * Register Customizer using jeg-framework
	 */
	public function customizer_option() {
		if ( class_exists( 'Jeg\Customizer\Customizer' ) ) {
			$this->customizer = \Jeg\Customizer\Customizer::get_instance();

			$this->set_section();
		}
	}

	/**
	 * Add new section in the panel
	 */
	public function set_section() {
		$ads_amp_section = array(
			'id'       => 'jnews_ads_amp_section',
			'title'    => esc_html__( 'AMP Ads', 'jnews-amp' ),
			'panel'    => 'jnews_ads',
			'priority' => 252,
			'type'     => 'jnews-lazy-section',
		);

		$this->customizer->add_section( $ads_amp_section );
	}

	/**
	 * Load Customizer Option
	 *
	 * @param $result
	 *
	 * @return mixed
	 */
	public function autoload_section( $result ) {
		$result['jnews_ads_amp_section'][] = JNEWS_AMP_DIR . 'include/class/customizer/options/amp-ads-option.php';
		return $result;
	}

		/**
		 * Load Customizer Option
		 *
		 * @param $result
		 *
		 * @return mixed
		 */
	public function amp_gdpr_options( $options ) {
		// $options = array_merge( $options, JNEWS_AMP_DIR . 'include/class/customizer/options/amp-gdpr-option.php' );
		// return $options;
		$options[] = array(
			'id'    => 'jnews_option[amp_gdpr_header}',
			'type'  => 'jnews-header',
			'label' => esc_html__( 'AMP GDPR Compliance', 'jnews-amp' ),
		);

		$options[] = array(
			'id'          => 'jnews_option[amp_gdpr_enable]',
			'option_type' => 'option',
			'transport'   => 'postMessage',
			'default'     => false,
			'type'        => 'jnews-toggle',
			'label'       => esc_html__( 'Enable GDPR Compliance', 'jnews-amp' ),
			'description' => esc_html__( 'Enable this option to show GDPR compliance notice on the AMP page.', 'jnews-amp' ),
		);

		$options[] = array(
			'id'              => 'jnews_option[amp_gdpr_heading]',
			'option_type'     => 'option',
			'transport'       => 'postMessage',
			'default'         => esc_html__( 'Headline', 'jnews-amp' ),
			'type'            => 'jnews-text',
			'label'           => esc_html__( 'Headline', 'jnews-amp' ),
			'description'     => esc_html__( 'Insert text for headline notice.', 'jnews-amp' ),
			'active_callback' => array(
				array(
					'setting'  => 'jnews_option[amp_gdpr_enable]',
					'operator' => '==',
					'value'    => true,
				),
			),
		);

		$options[] = array(
			'id'              => 'jnews_option[amp_gdpr_text]',
			'option_type'     => 'option',
			'transport'       => 'postMessage',
			'default'         => esc_html__( 'This is an important message requiring you to make a choice if you\'re based in the EU.', 'jnews-amp' ),
			'type'            => 'jnews-textarea',
			'label'           => esc_html__( 'Notice Content', 'jnews-amp' ),
			'description'     => esc_html__( 'Insert text for the notice content.', 'jnews-amp' ),
			'active_callback' => array(
				array(
					'setting'  => 'jnews_option[amp_gdpr_enable]',
					'operator' => '==',
					'value'    => true,
				),
			),
		);

		$options[] = array(
			'id'              => 'jnews_option[amp_gdpr_accept]',
			'option_type'     => 'option',
			'transport'       => 'postMessage',
			'default'         => esc_html__( 'Accept', 'jnews-amp' ),
			'type'            => 'jnews-text',
			'label'           => esc_html__( 'Accept Text', 'jnews-amp' ),
			'description'     => esc_html__( 'Insert text for accept button.', 'jnews-amp' ),
			'active_callback' => array(
				array(
					'setting'  => 'jnews_option[amp_gdpr_enable]',
					'operator' => '==',
					'value'    => true,
				),
			),
		);

		$options[] = array(
			'id'              => 'jnews_option[amp_gdpr_reject]',
			'option_type'     => 'option',
			'transport'       => 'postMessage',
			'default'         => esc_html__( 'Reject', 'jnews-amp' ),
			'type'            => 'jnews-text',
			'label'           => esc_html__( 'Reject Text', 'jnews-amp' ),
			'description'     => esc_html__( 'Insert text for reject button.', 'jnews-amp' ),
			'active_callback' => array(
				array(
					'setting'  => 'jnews_option[amp_gdpr_enable]',
					'operator' => '==',
					'value'    => true,
				),
			),
		);

		$options[] = array(
			'id'              => 'jnews_option[amp_gdpr_setting]',
			'option_type'     => 'option',
			'transport'       => 'postMessage',
			'default'         => esc_html__( 'Update Consent', 'jnews-amp' ),
			'type'            => 'jnews-text',
			'label'           => esc_html__( 'Setting Text', 'jnews-amp' ),
			'description'     => esc_html__( 'Insert text for setting button.', 'jnews-amp' ),
			'active_callback' => array(
				array(
					'setting'  => 'jnews_option[amp_gdpr_enable]',
					'operator' => '==',
					'value'    => true,
				),
			),
		);

		$options[] = array(
			'id'              => 'jnews_option[amp_gdpr_privacy]',
			'option_type'     => 'option',
			'transport'       => 'postMessage',
			'type'            => 'jnews-text',
			'label'           => esc_html__( 'Privacy Policy', 'jnews-amp' ),
			'description'     => esc_html__( 'Insert text for privacy policy link.', 'jnews-amp' ),
			'active_callback' => array(
				array(
					'setting'  => 'jnews_option[amp_gdpr_enable]',
					'operator' => '==',
					'value'    => true,
				),
			),
		);

		$options[] = array(
			'id'              => 'jnews_option[amp_gdpr_privacy_url]',
			'option_type'     => 'option',
			'transport'       => 'postMessage',
			'type'            => 'jnews-text',
			'label'           => esc_html__( 'Privacy Policy URL', 'jnews-amp' ),
			'description'     => esc_html__( 'Insert url for privacy policy link.', 'jnews-amp' ),
			'active_callback' => array(
				array(
					'setting'  => 'jnews_option[amp_gdpr_enable]',
					'operator' => '==',
					'value'    => true,
				),
			),
		);

		$options[] = array(
			'id'              => 'jnews_option[amp_gdpr_fontawesome]',
			'option_type'     => 'option',
			'transport'       => 'postMessage',
			'default'         => false,
			'type'            => 'jnews-toggle',
			'label'           => esc_html__( 'Load FontAwesome Locally', 'jnews-amp' ),
			'description'     => esc_html__( 'Enable this option to force load FontAwesome locally.', 'jnews-amp' ),
			'active_callback' => array(
				array(
					'setting'  => 'jnews_option[amp_gdpr_enable]',
					'operator' => '==',
					'value'    => true,
				),
			),
		);

		return $options;
	}


}

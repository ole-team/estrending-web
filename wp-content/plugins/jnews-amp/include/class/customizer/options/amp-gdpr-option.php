<?php
$options[] = array(
	'id'    => 'jnews_option[amp_gdpr_header}',
	'type'  => 'jnews-header',
	'label' => esc_html__( 'AMP GDPR Compliance', 'jnews-amp' ),
);

$options[] = array(
	'id'          => 'jnews_option[amp_gdpr_enable]',
	'option_type' => 'option',
	'transport'   => 'postMessage',
	'default'     => false,
	'type'        => 'jnews-toggle',
	'label'       => esc_html__( 'Enable GDPR Compliance', 'jnews-amp' ),
	'description' => esc_html__( 'Enable this option to show GDPR compliance notice on the AMP page.', 'jnews-amp' ),
);

$options[] = array(
	'id'              => 'jnews_option[amp_gdpr_heading]',
	'option_type'     => 'option',
	'transport'       => 'postMessage',
	'default'         => esc_html__( 'Headline', 'jnews-amp' ),
	'type'            => 'jnews-text',
	'label'           => esc_html__( 'Headline', 'jnews-amp' ),
	'description'     => esc_html__( 'Insert text for headline notice.', 'jnews-amp' ),
	'active_callback' => array(
		array(
			'setting'  => 'jnews_option[amp_gdpr_enable]',
			'operator' => '==',
			'value'    => true,
		),
	),
);

$options[] = array(
	'id'              => 'jnews_option[amp_gdpr_text]',
	'option_type'     => 'option',
	'transport'       => 'postMessage',
	'default'         => esc_html__( 'This is an important message requiring you to make a choice if you\'re based in the EU.', 'jnews-amp' ),
	'type'            => 'jnews-textarea',
	'label'           => esc_html__( 'Notice Content', 'jnews-amp' ),
	'description'     => esc_html__( 'Insert text for the notice content.', 'jnews-amp' ),
	'active_callback' => array(
		array(
			'setting'  => 'jnews_option[amp_gdpr_enable]',
			'operator' => '==',
			'value'    => true,
		),
	),
);

$options[] = array(
	'id'              => 'jnews_option[amp_gdpr_accept]',
	'option_type'     => 'option',
	'transport'       => 'postMessage',
	'default'         => esc_html__( 'Accept', 'jnews-amp' ),
	'type'            => 'jnews-text',
	'label'           => esc_html__( 'Accept Text', 'jnews-amp' ),
	'description'     => esc_html__( 'Insert text for accept button.', 'jnews-amp' ),
	'active_callback' => array(
		array(
			'setting'  => 'jnews_option[amp_gdpr_enable]',
			'operator' => '==',
			'value'    => true,
		),
	),
);

$options[] = array(
	'id'              => 'jnews_option[amp_gdpr_reject]',
	'option_type'     => 'option',
	'transport'       => 'postMessage',
	'default'         => esc_html__( 'Reject', 'jnews-amp' ),
	'type'            => 'jnews-text',
	'label'           => esc_html__( 'Reject Text', 'jnews-amp' ),
	'description'     => esc_html__( 'Insert text for reject button.', 'jnews-amp' ),
	'active_callback' => array(
		array(
			'setting'  => 'jnews_option[amp_gdpr_enable]',
			'operator' => '==',
			'value'    => true,
		),
	),
);

$options[] = array(
	'id'              => 'jnews_option[amp_gdpr_setting]',
	'option_type'     => 'option',
	'transport'       => 'postMessage',
	'default'         => esc_html__( 'Update Consent', 'jnews-amp' ),
	'type'            => 'jnews-text',
	'label'           => esc_html__( 'Setting Text', 'jnews-amp' ),
	'description'     => esc_html__( 'Insert text for setting button.', 'jnews-amp' ),
	'active_callback' => array(
		array(
			'setting'  => 'jnews_option[amp_gdpr_enable]',
			'operator' => '==',
			'value'    => true,
		),
	),
);

$options[] = array(
	'id'              => 'jnews_option[amp_gdpr_privacy]',
	'option_type'     => 'option',
	'transport'       => 'postMessage',
	'type'            => 'jnews-text',
	'label'           => esc_html__( 'Privacy Policy', 'jnews-amp' ),
	'description'     => esc_html__( 'Insert text for privacy policy link.', 'jnews-amp' ),
	'active_callback' => array(
		array(
			'setting'  => 'jnews_option[amp_gdpr_enable]',
			'operator' => '==',
			'value'    => true,
		),
	),
);

$options[] = array(
	'id'              => 'jnews_option[amp_gdpr_privacy_url]',
	'option_type'     => 'option',
	'transport'       => 'postMessage',
	'type'            => 'jnews-text',
	'label'           => esc_html__( 'Privacy Policy URL', 'jnews-amp' ),
	'description'     => esc_html__( 'Insert url for privacy policy link.', 'jnews-amp' ),
	'active_callback' => array(
		array(
			'setting'  => 'jnews_option[amp_gdpr_enable]',
			'operator' => '==',
			'value'    => true,
		),
	),
);

$options[] = array(
	'id'              => 'jnews_option[amp_gdpr_fontawesome]',
	'option_type'     => 'option',
	'transport'       => 'postMessage',
	'default'         => false,
	'type'            => 'jnews-toggle',
	'label'           => esc_html__( 'Load FontAwesome Locally', 'jnews-amp' ),
	'description'     => esc_html__( 'Enable this option to force load FontAwesome locally.', 'jnews-amp' ),
	'active_callback' => array(
		array(
			'setting'  => 'jnews_option[amp_gdpr_enable]',
			'operator' => '==',
			'value'    => true,
		),
	),
);

return $options;

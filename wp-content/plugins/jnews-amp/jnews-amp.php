<?php
/*
	Plugin Name: JNews - AMP Extender
	Plugin URI: http://jegtheme.com/
	Description: Extend WordPress AMP to fit with JNews Style
	Version: 10.0.1
	Author: Jegtheme
	Author URI: http://jegtheme.com
	License: GPL2
*/

defined( 'JNEWS_AMP' ) || define( 'JNEWS_AMP', 'jnews-amp' );
defined( 'JNEWS_AMP_VERSION' ) || define( 'JNEWS_AMP_VERSION', '10.0.0' );
defined( 'JNEWS_AMP_URL' ) || define( 'JNEWS_AMP_URL', plugins_url( JNEWS_AMP ) );
defined( 'JNEWS_AMP_FILE' ) || define( 'JNEWS_AMP_FILE', __FILE__ );
defined( 'JNEWS_AMP_DIR' ) || define( 'JNEWS_AMP_DIR', plugin_dir_path( __FILE__ ) );
defined( 'JNEWS_AMP_CLASSPATH' ) || define( 'JNEWS_AMP_CLASSPATH', JNEWS_AMP_DIR . 'include/class/' );

require_once JNEWS_AMP_DIR . 'include/autoload.php';

function JNews_AMP() {
	static $instance;

	if ( null === $instance || ! ( $instance instanceof JNews\AMP\Init ) ) {
		$instance = JNews\AMP\Init::instance();
	}
	return $instance;
}

JNews_AMP();

Change Log :

== 10.0.1 ==
- [IMPROVEMENT] add calories option for recipe schema
- [BUG] Fix error warning when preparation and cook time is empty

== 10.0.0 ==
- [IMPROVEMENT] Compatible with JNews v10.0.0

== 9.0.2 ==
- [BUG] Fix PHP 5.6 issue

== 9.0.1 ==
- [IMPROVEMENT] Add recipeYield schema

== 9.0.0 ==
- [IMPROVEMENT] Compatible with JNews v9.0.0

== 8.0.1 ==
- [BUG] Fix deprecation warning issue

== 8.0.0 ==
- [IMPROVEMENT] Compatible with JNews v8.0.0

== 7.0.2 ==
- [IMPROVEMENT] Use .on() and .off()

== 7.0.1 ==
- [BUG] Fix contentURL video schema

== 7.0.0 ==
- [IMPROVEMENT] Compatible with JNews v7.0.0

== 6.0.1 ==
- [IMPROVEMENT] Only load the style and script file when needed

== 6.0.0 ==
- [IMPROVEMENT] Compatible with JNews v6.0.0

== 5.0.0 ==
- [IMPROVEMENT] Compatible with JNews v5.0.0

== 4.0.0 ==
- [IMPROVEMENT] Compatible with JNews v4.0.0

== 3.0.1 ==
- [IMPROVEMENT] Adding some input field for Google Structured Data

== 3.0.0 ==
- [IMPROVEMENT] Compatible with JNews v3.0.0

== 2.0.0 ==
- [IMPROVEMENT] Compatible with JNews v2.0.0

== 1.0.2 ==
- [BUG] Fix round number issue

== 1.0.1 ==
- [BUG] Fix wrong file name

== 1.0.0 ==
- First Release

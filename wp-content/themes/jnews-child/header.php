<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9]>    <html class="no-js lt-ie10" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=yes' />
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="shortcut icon" href="https://media.estrending.com/wp-content/uploads/2022/11/etfavicon.png" />
    <!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-BC22RWTZ77"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-BC22RWTZ77');
  gtag('config', 'UA-64020906-1');
</script>
    <?php if(!is_404()){ 
    if(is_single()){
        if(has_tag('noads') or has_tag('branded')){ }else{
        ?>
        <script async src="//js-sec.indexww.com/ht/p/189664-267668706674117.js"></script>
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script src="https://jsc.mgid.com/site/725954.js" async></script>
        <?php  }
    }else{ ?>
    <script async src="//js-sec.indexww.com/ht/p/189664-267668706674117.js"></script>
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
    <?php }
    } ?>

    <?php wp_head(); ?>

<script data-ad-client="ca-pub-9578846297626580" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Meta Pixel Code -->

<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '390851043572523');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=390851043572523&ev=PageView&noscript=1"/></noscript>
<!-- End Meta Pixel Code -->
</head>
<body <?php body_class(); ?>>

    <?php do_action('jnews_after_body'); ?>

    <?php get_template_part('fragment/side-feed'); ?>

    <div class="jeg_ad jeg_ad_top jnews_header_top_ads">
        <?php do_action('jnews_header_top_ads'); ?>
    </div>

    <!-- The Main Wrapper
    ============================================= -->
    <div class="jeg_viewport">

        <?php jnews_background_ads(); ?>

        <div class="jeg_header_wrapper">
            <?php get_template_part('fragment/header/desktop-builder'); ?>
        </div>

        <div class="jeg_header_sticky">
            <?php get_template_part('fragment/header/desktop-sticky-wrapper'); ?>
        </div>

        <div class="jeg_navbar_mobile_wrapper">
            <?php get_template_part('fragment/header/mobile-builder'); ?>
        </div>
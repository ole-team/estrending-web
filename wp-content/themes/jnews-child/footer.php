        <div class="footer-holder" id="footer" data-id="footer">
            <?php
                $footer_style = get_theme_mod('jnews_footer_style', '1');
                if($footer_style === 'custom') {
                    get_template_part('fragment/footer/footer-custom');
                } else {
                    get_template_part('fragment/footer/footer-' . $footer_style);
                }
            ?>
        </div>

        <div class="jscroll-to-top">
        	<a href="#back-to-top" class="jscroll-to-top_link"><i class="fa fa-angle-up"></i></a>
        </div>
    </div>

    <?php
        get_template_part('fragment/header/mobile-menu');

        if(apply_filters('jnews_can_render_account_popup', false))
        {
            get_template_part('fragment/account/account-popup');
        }

        wp_footer();
    ?>
<?php if(has_tag('branded')){
    //avoid Ads
}else{ ?>
<div id="wrap_footer_banner" style="position:fixed; bottom:0px; width:fit-content; z-index:9999; left: 50%; transform: translateX(-50%); text-align:center; display: none;">
    <div id='footbanner'>
        <script>
            googletag.cmd.push(function() { googletag.display('footbanner'); });
        </script>
    </div>
    <div id="close_banner_ft" style="position: absolute; border-radius: 25px; background-color: #000; color: #fff; width: 25px; height: 25px; font-size: 20px; text-align: center;  top: -10px; left: 0px;  cursor: pointer;">x</div>
    </div>
<div id="overlay_1x1">
    <script>
    googletag.cmd.push(function() { googletag.display("overlay_1x1"); });
    </script>
</div>
<?php } ?>
<?php if ( is_front_page() ) :?>
<script>
jQuery(document).ready(function($){
    $('.dm_video_list .dm_item_side').click(function(){
        $('.dm_video_list .dm_item_side').removeClass('currentdm');
        $(this).addClass('currentdm');
        var videoidm = $(this).attr('data-id');
        var updatevidm = 'https://www.dailymotion.com/embed/video/'+videoidm+'?ads_params=category%253Dhome%2526site%3Damericadigital&origin=https%3A%2F%2Fwww.americadigital.com&queue-autoplay-next=false&queue-enable=false&sharing-enable=false';
        $('.dm_video_container iframe').attr('src',updatevidm);
    });
});
</script>
<?php endif; ?>
<?php if ( is_single() ) { ?>
<?php if(!has_tag('split')){?>
<div id="intext_1x1">
    <script>
    googletag.cmd.push(function() { googletag.display("intext_1x1"); });
    </script>
</div>
<?php } ?>
<script>
jQuery(document).ready(function($){
    var itemvideod = $('div[id*=-DLM]');
    itemvideod.each(function(i){
        var idDM = $(this).attr('data-id')
        var elementPlayer = $(this).attr('id');
        var controlPlayer = 0;
        if(idDM){
            window['playerContent'+i] = DM.player(document.getElementById(elementPlayer), {
                video: idDM, 
                width: "100%", 
                height: "100%", 
                params: { 
                    autoplay: true, 
                    mute: true, 
                    "queue-enable": false, 
                    "queue-autoplay-next": false, 
                    "sharing-enable":false, 
                } 
            }); 
            window['playerContent'+i].addEventListener("play",function(e){
                if(controlPlayer < 1){
                    gtag('event', 'Play', {
                      'event_label': 'video-'+idDM,
                      'event_category': 'Videos',
                    });
                  controlPlayer = 1;
                }
            }); 
        }
    });
    $('.jnews_related_post_container .jeg_block_heading h3 span').html('Te puede interesar');
    number_paragraphs = $('.content-inner').find('p').size();
    distribuiteP = Math.round(number_paragraphs / 2);
    <?php if(!has_tag('branded')){ ?>
    $.each($('.content-inner p'), function (index, paragraph) {
        if(distribuiteP >= 2){
            var controlp = distribuiteP;
            if (index == controlp) {
              $(paragraph).before('<div id="midbanner" style="text-align:center; margin-left: -10px;"></div>');
              googletag.cmd.push(function() { googletag.display("midbanner"); });
            }
        }else{
            if (index == 3) { 
              $(paragraph).after('<div id="midbanner" style="text-align:center; margin-left: -10px;"></div>');
              googletag.cmd.push(function() { googletag.display("midbanner"); });
            }
        }
    });
    <?php } ?>
    <?php if(has_tag('split')){ ?>
    $(window).scroll(function(){
    var scroll = $(window).scrollTop();
    var pageBottom = scroll + $(window).height();
    var elementTop = Math.round($(".jeg_singlepage").offset().top);
    var elementBottom = elementTop + $(".jeg_singlepage").height()+100;
    var esteY = elementTop - scroll;
    var elementH = Math.round($(".content-inner").height());
    
    var documentH = $(document).height();
    var visible = $(document).height() - scroll;
    var windowT = $(window).height()+200;
    var controlVi = documentH - visible;
    if (scroll >= elementTop) {
        $('.sky-right').css('position','fixed');
        $('.sky-left').css('position','fixed');
        if(visible >= windowT){
            $('.sky-right, .sky-left').css('bottom','200px');
            $('.sky-right,.sky-left').css('top','50px');
        }else{
            $('.sky-right,.sky-left').css('bottom','200px');
            $('.sky-right,.sky-left').css('top','auto');
        }
    }else{
        $('.sky-left').css('position','static');
        $('.sky-right').css('position','static');
    }
    });
    <?php } ?>
});
</script>
<?php } ?>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('#close_banner_ft').on('click',function(){
            $('#wrap_footer_banner').remove();
        });
    });
</script>
</body>
</html>
